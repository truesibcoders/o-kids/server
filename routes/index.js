let router = require('express').Router();

// index
router.get('/', function (req, res) {
    res.render('index', {title: 'O\'Kids server'})
});
router.get('/favicon.ico', function (req, res) {
});

// query
let account = require('../query/account'),
    mentor = require('../query/mentor'),
    child = require('../query/child'),
    event = require('../query/event'),
    program = require('../query/program');

// account
router.post('/api/account/create', (req, res, next) => {
    account.createAccount(req, res, next);
    mentor.createMentor(req, res, next);
    mentor.getMentor(req, res, next)
});

// mentor
router.post('/api/mentor/get', mentor.getMentor);
router.post('/api/profile/get', mentor.getProfile);

// child
router.post('/api/child/create', child.createChildren);
router.post('/api/child/get', child.getChildren);

// event
router.post('/api/event/getByEventType', event.getEventByEventType);
router.post('/api/event/getById', event.getEventById);
router.post('/api/event/getAll', event.getAllEvent);

// program
router.post('/api/program/getById', program.getProgramById);
router.post('/api/program/getAll', program.getAllProgram);

module.exports = router;
