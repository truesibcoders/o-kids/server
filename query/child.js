let config = require('../config/config');
let dateFormat = require('dateformat');

let db = config.db;

// Methods
module.exports = {
    getChildren: getChildren,
    createChildren: createChildren
};

// получить детей
function getChildren(req, res, next) {
    return db.any(
        'SELECT ch.id, type_of_deviation_id, ch.name, ch.surname, age, sex, birthday ' +
        'FROM child ch ' +
        'INNER JOIN mentor m ON ch.mentor_id = m.id ' +
        'WHERE m.id = $1', req.body.mentor_id)
        .then(children => {
            children.forEach(child => {
                child.birthday = dateFormat(child.birthday, "dd.mm.yyyy");
            });
            res.status(200)
                .json({
                    status: 'success',
                    children
                })
        })
        .catch(err => {
            return next(err)
        })
}

// создать ребенка
function createChildren(req, res, next) {
    return db.any(
        'INSERT INTO child ' +
        '(mentor_id, type_of_deviation_id, name, surname, age, sex, birthday) VALUES' +
        '($1, $2, $3, $4, $5, $6,$7)',
        [req.body.mentor_id, req.body.type_of_deviation_id, req.body.name, req.body.surname, req.body.age, req.body.sex, req.body.birthday])
        .then(
            res.status(200)
                .json({
                    status: 'success'
                })
        ).catch(err => {
            return next(err)
        })
}
