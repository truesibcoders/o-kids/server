let config = require('../config/config');
let dateFormat = require('dateformat');

let db = config.db;

// Methods
module.exports = {
    getAllEvent: getAllEvent,
    getEventById: getEventById,
    getEventByEventType: getEventByEventType
};

function getAllEvent(req, res, next) {
    return db.any(
        'SELECT e.id, e.event_type_id, e.title, date_of_event, organizer, place, description, number_of_seats, min_age, max_age, photo_link, tod.name AS type_of_deviation_name ' +
        'FROM event e ' +
        'INNER JOIN event_type et ON et.id = e.event_type_id ' +
        'INNER JOIN type_of_deviation tod ON tod.id = e.type_of_deviation_id ' +
        'WHERE date_of_event >= current_date')
        .then(events => {
            events.forEach(event => {
                event.date_of_event = dateFormat(event.date_of_event, "dd.mm.yyyy");
            });
            res.status(200)
                .json({
                    status: 'success',
                    events
                })
        })
        .catch(err => {
            return next(err)
        })
}

function getEventByEventType(req, res, next) {
    return db.any(
        'SELECT e.title, date_of_event, organizer, place, description, number_of_seats, min_age, max_age, photo_link ' +
        'FROM event e ' +
        'INNER JOIN event_type et ON et.id = event_type_id ' +
        'WHERE date_of_event >= current_date AND et.id = $1', req.body.event_type_id)
        .then(event => {
            res.status(200)
                .json({
                    status: 'success',
                    account: event
                })
        })
        .catch(err => {
            return next(err)
        })
}

function getEventById(req, res, next) {
    return db.any(
        'SELECT e.title, e.date_of_event, e.organizer, e.place, e.description, e.number_of_seats, e.min_age, e.max_age, e.photo_link, e.type_of_deviation_id ' +
        'FROM event e ' +
        'INNER JOIN event_type et ON et.id = event_type_id ' +
        'WHERE e.id = $1', req.body.id)
        .then(event => {
            res.status(200)
                .json({
                    status: 'success',
                    account: event
                })
        })
        .catch(err => {
            return next(err)
        })
}

