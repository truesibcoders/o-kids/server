let config = require('../config/config');

let db = config.db;

// Methods
module.exports = {
    getAllProgram: getAllProgram,
    getProgramById: getProgramById,
};

function getAllProgram(req, res, next) {
    return db.any(
        'SELECT p.id, p.title, p.description, p.min_age, p.max_age, tod.id AS type_of_deviation_id, tod.name AS type_of_deviation_name, p.photo_link, ct.title AS complexity_type_name, pt.title AS program_type_name, pea.title AS program_edu_area_name ' +
        'FROM program p ' +
        'INNER JOIN type_of_deviation tod ON tod.id = p.type_of_deviation_id '+
        'INNER JOIN program_type pt ON pt.id = p.program_type_id '+
        'INNER JOIN complexity_type ct ON ct.id = p.complexity_id ' +
        'INNER JOIN program_edu_area pea ON pea.id = p.program_edu_area_id ' +
        'ORDER BY p.id DESC')
        .then(programs => {
            res.status(200)
                .json({
                    status: 'success',
                    programs
                })
        })
        .catch(err => {
            return next(err)
        })
}

function getProgramById(req, res, next) {
    return db.any(
        'SELECT e.date_of_event, e.organizer, e.place, e.description, e.number_of_seats, e.min_age, e.max_age, e.photo_link ' +
        'FROM event e ' +
        'INNER JOIN event_type et ON et.id = event_type_id ' +
        'WHERE e.id = $1', req.body.id)
        .then(program => {
            res.status(200)
                .json({
                    status: 'success',
                    account: program
                })
        })
        .catch(err => {
            return next(err)
        })
}

