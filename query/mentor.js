let config = require('../config/config');

let db = config.db;

// Methods
module.exports = {
    getMentor: getMentor,
    getProfile: getProfile,
    createMentor: createMentor
};

// получить ментора
function getMentor(req, res, next) {
    return db.one(
        'SELECT m.id, m.name, m.surname, m.patronymic, m.email, m.phone_number, a.token ' +
        'FROM mentor m ' +
        'INNER JOIN account a ON a.id = m.id ' +
        'WHERE a.username = $1 AND a.password = $2',
        [req.body.username, req.body.password])
        .then(mentor => {
            res.status(200)
                .json({
                    status: 'success',
                    mentor
                })
        })
        .catch(err => {
            return next(err)
        })
}

function getProfile(req, res, next) {
    return db.one(
        'SELECT m.id, m.name, m.surname, m.patronymic, m.email, m.phone_number, a.token ' +
        'FROM mentor m ' +
        'INNER JOIN account a ON a.id = m.id ' +
        'WHERE m.id = $1',
        req.body.id)
        .then(mentor => {
            res.status(200)
                .json({
                    status: 'success',
                    mentor
                })
        })
        .catch(err => {
            return next(err)
        })
}

// создать ментора
function createMentor(req, res, next) {
    return db.any(
        'INSERT INTO mentor ' +
        '(name, surname, patronymic, email, phone_number) VALUES' +
        '($1, $2, $3, $4, $5)',
        [req.body.name, req.body.surname, req.body.patronymic, req.body.email, req.body.phone_number])
        .catch(err => {
            return next(err)
        })
}
