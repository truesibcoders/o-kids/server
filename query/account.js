let config = require('../config/config');

let db = config.db;

// Methods
module.exports = {
    createAccount: createAccount
};

// регистрация
function createAccount(req, res, next) {
    return db.any(
        'INSERT INTO account ' +
        '(username, password, token) VALUES' +
        '($1, $2, $3)',
        [req.body.username, req.body.password, Math.random().toString(36).substring(2, 15)])
        .catch(err => {
            return next(err)
        })
}
